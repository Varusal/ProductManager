﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;
using ProductManager.Classes;

namespace ProductManager.Windows
{
    /// <summary>
    /// Interaction logic for ExImView.xaml
    /// </summary>
    public partial class ExImView : Window
    {
        string dir;
        string file;
        OpenFileDialog fileDia;
        FolderBrowserDialog folderDia;
        public ExImView(string dir)
        {
            InitializeComponent();
            this.dir = dir;
            file = default(string);
            fileDia = new OpenFileDialog();
            folderDia = new FolderBrowserDialog();
        }

        private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)rbExport.IsChecked)
            {
                folderDia.ShowDialog();
                tbxExIm.Text = folderDia.SelectedPath;
                file = folderDia.SelectedPath + @"\products.json";
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                File.Copy(dir, file);
            }
            else if ((bool)rbImport.IsChecked)
            {
                fileDia.ShowDialog();
                tbxExIm.Text = fileDia.FileName;
                if (File.Exists(dir))
                {
                    File.Delete(dir);
                }
                File.Copy(fileDia.FileName, dir);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}
