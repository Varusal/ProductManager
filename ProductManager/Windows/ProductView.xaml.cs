﻿using System.Windows;
using ProductManager.Classes;

namespace ProductManager.Windows
{
    /// <summary>
    /// Interaction logic for ProductView.xaml
    /// </summary>
    public partial class ProductView : Window
    {
        EnvironmentBuilder envBuilder;
        JsonGenerator jsonGen;
        JsonReader jsonReader;
        string dir;
        ExImView exImView;
        public ProductView()
        {
            InitializeComponent();
            envBuilder = new EnvironmentBuilder();
            dir = envBuilder.Directory();
            jsonGen = new JsonGenerator(dir);
            jsonReader = new JsonReader(dir, this);
            dgProducts.ItemsSource = jsonReader.ReadJson();
            exImView = new ExImView(dir + @"\products.json");
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (tbKey.Text != string.Empty && tbProduct.Text != string.Empty)
            {
                jsonGen.GenerateJson(tbProduct.Text, tbKey.Text);
                tbKey.Clear();
                tbProduct.Clear();
            }
            dgProducts.ItemsSource = jsonReader.ReadJson();
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                btnAdd_Click(sender, e);
            }
        }

        private void btnExIm_Click(object sender, RoutedEventArgs e)
        {
            exImView.ShowDialog();
        }
    }
}
