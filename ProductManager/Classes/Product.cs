﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManager.Classes
{
    public class Product
    {
        string productName;
        string key;

        public string ProductName { get => productName; set => productName = value; }
        public string Key { get => key; set => key = value; }
    }
}
