﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace ProductManager.Classes
{
    class JsonReader
    {
        string dir;
        Windows.ProductView prodView;
        public JsonReader(string dir, Windows.ProductView prodView)
        {
            this.dir = dir + @"\products.json";
            this.prodView = prodView;
        }

        public ObservableCollection<Product> ReadJson()
        {
            try
            {
                ObservableCollection<Product> prods = new ObservableCollection<Product>();
                StreamReader reader = new StreamReader(dir);
                var jsonData = reader.ReadToEnd();
                List<Product> items = JsonConvert.DeserializeObject<List<Product>>(jsonData);
                foreach (var item in items)
                {
                    prods.Add(new Product { ProductName = item.ProductName, Key = item.Key });
                }
                return prods;
            }
            catch
            {
                return null;
            }
        }
    }
}
