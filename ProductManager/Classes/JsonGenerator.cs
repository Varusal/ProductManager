﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace ProductManager.Classes
{
    class JsonGenerator
    {
        Product prod;
        public string dir;
        EnvironmentBuilder envBuilder;
        string content;
        public JsonGenerator(string dir)
        {
            this.dir = dir + @"\products.json";
            envBuilder = new EnvironmentBuilder();
        }

        public void GenerateJson(string product, string key)
        {
            prod = new Product
            {
                ProductName = product,
                Key = key
            };
            ReadFile();
        }

        private void ReadFile()
        {
            if (!File.Exists(dir))
            {
                File.Create(dir).Close();
                File.WriteAllText(dir, "[");
            }
            content = File.ReadAllText(dir);
            if (content[content.Length - 1] == ']')
            {
                content = content.Remove(content.Length - 1) + ",";
            }
            content += JsonConvert.SerializeObject(prod, Formatting.Indented) + "]";
            File.WriteAllText(dir, content);
        }
    }
}
