﻿using System;

namespace ProductManager.Classes
{
    class EnvironmentBuilder
    {
        string dir = null;

        public EnvironmentBuilder()
        {
            dir = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ProductManager");
        }
        public string Directory()
        {
            switch (System.IO.Directory.Exists(dir))
            {
                case false:
                    System.IO.Directory.CreateDirectory(dir);
                    break;
            }
            return dir;
        }
    }
}
